/// @file Socket_handler.cpp

#include <Socket_handler.h>
#include <Stoppable.h>

#include <cstring>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

namespace Katas
{

//-----------------------------------------------------------------------------

struct Socket_handler::Impl : public Stoppable
{
  int m_socket{0};
  bool m_is_set_up{true};
  socket_type m_socket_type{socket_type::unknown};
  int m_socket_fd{0};
  int m_port{0};
  socket_handler_callback_t m_socket_handler_callback{nullptr};
  void *m_socket_handler_context{nullptr};
  std::thread *m_p_thread{nullptr};
  struct sockaddr_in m_server_address;
  size_t m_buffer_size{0};
  std::vector<uint8_t> m_buffer;

  Impl(const size_t buffer_size);

  ~Impl();

  bool set_up_UDP_server(const std::string &IP_address, const int port);

  bool server_read_from_socket();

  bool set_up_UDP_client(const std::string &IP_address, const int port);

  bool client_send_to_socket(const uint8_t *buffer,
                             const size_t number_of_bytes);

  Impl(Impl &&) = delete;
  Impl(const Impl &) = delete;
  Impl &operator=(Impl &&) = delete;
  Impl &operator=(const Impl &) = delete;
};

//-----------------------------------------------------------------------------

Socket_handler::Socket_handler(const socket_type type,
                               const std::string &IP_address, const int port,
                               const size_t buffer_size)
    : m_p_impl(std::make_unique<Impl>(buffer_size))
{
  m_p_impl->m_socket_type = type;
  if (socket_type::UDP_server == type)
  {
    m_p_impl->set_up_UDP_server(IP_address, port);
  }
  if (socket_type::UDP_client == type)
  {
    m_p_impl->set_up_UDP_client(IP_address, port);
  }
}

Socket_handler::Impl::Impl(const size_t buffer_size)
    : m_server_address(), m_buffer_size(buffer_size), m_buffer(buffer_size)
{
}

Socket_handler::~Socket_handler()
{
}

Socket_handler::Impl::~Impl()
{
  if (-1 < m_socket_fd)
  {
    close(m_socket_fd);
  }
}

bool Socket_handler::is_set_up()
{
  return m_p_impl->m_is_set_up;
}

void Socket_handler::register_callback(const socket_handler_callback_t callback,
                                       void *context)
{
  m_p_impl->m_socket_handler_callback = callback;
  m_p_impl->m_socket_handler_context = context;
}

bool Socket_handler::send_data(const uint8_t *buffer,
                               const size_t number_of_bytes)
{
  return m_p_impl->client_send_to_socket(buffer, number_of_bytes);
}

bool Socket_handler::Impl::set_up_UDP_server(const std::string &IP_address,
                                             const int port)
{
  m_port = port;
  m_socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (-1 == m_socket_fd)
  {
    perror("socket failed");
    m_is_set_up = false;
  }

  if (m_is_set_up)
  {
    m_server_address.sin_family = AF_INET;
    m_server_address.sin_port = htons(m_port);
    m_server_address.sin_addr.s_addr = htons(INADDR_ANY);

    if (-1 == bind(m_socket_fd, (struct sockaddr *)&m_server_address,
                   sizeof(m_server_address)))
    {
      perror("bind failed");
      m_is_set_up = false;
    }
  }
  if (m_is_set_up)
  {
    auto th = new std::thread([&]() { server_read_from_socket(); });
  }
  return m_is_set_up;
}

bool Socket_handler::Impl::server_read_from_socket()
{
  struct sockaddr_in client_address = {0};
  if (m_is_set_up)
  {
    bool keep_going = true;
    while (!stop_requested())
    {
      socklen_t len = sizeof(client_address);

      int number_of_bytes_read = recvfrom(
          m_socket_fd, static_cast<void *>(m_buffer.data()), m_buffer_size,
          MSG_WAITALL, (struct sockaddr *)&client_address, &len);
      if (-1 == number_of_bytes_read)
      {
        perror("recvfrom error");
      }
      else if (nullptr != m_socket_handler_callback)
      {
        m_socket_handler_callback(m_socket_handler_context,
                                  static_cast<uint8_t *>(m_buffer.data()),
                                  number_of_bytes_read);
      }
    }
  }
  return m_is_set_up;
}

bool Socket_handler::Impl::set_up_UDP_client(const std::string &IP_address,
                                             const int port)
{
  m_port = port;
  m_socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (0 > m_socket_fd)
  {
    perror("socket failed");
    m_is_set_up = false;
  }
  if (m_is_set_up)
  {

    m_server_address.sin_family = AF_INET;
    m_server_address.sin_port = htons(m_port);
    if (0 >= inet_pton(AF_INET, IP_address.c_str(), &m_server_address.sin_addr))
    {
      perror("inet_pton failed");
      m_is_set_up = false;
    }
  }
  return m_is_set_up;
}

bool Socket_handler::Impl::client_send_to_socket(const uint8_t *buffer,
                                                  const size_t number_of_bytes)
{
  if (m_is_set_up)
  {
    int n = sendto(m_socket_fd, buffer, number_of_bytes, MSG_CONFIRM,
                   (const struct sockaddr *)&m_server_address,
                   sizeof(m_server_address));
  }
  return m_is_set_up;
}

} // namespace Katas
