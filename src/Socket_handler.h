/// @file Socket_handler.h

#ifndef SRC_SOCKET_HANDLER_H
#define SRC_SOCKET_HANDLER_H

#include <memory>
#include <string>

namespace Katas
{

enum class socket_type
{
  unknown,
  UDP_client,
  UDP_server
};

using socket_handler_callback_t = void (*)(void *context, uint8_t *buffer,
                                           size_t number_of_bytes);

class Socket_handler
{
public:
  Socket_handler(const socket_type type, const std::string &IP_address,
                 const int port, const size_t buffer_size);

  ~Socket_handler();

  bool is_set_up();

  void register_callback(const socket_handler_callback_t callback, void *context);

  bool send_data(const uint8_t *buffer, const size_t number_of_bytes);

  Socket_handler() = delete;
  Socket_handler(Socket_handler &&) = delete;
  Socket_handler(const Socket_handler &) = delete;
  Socket_handler &operator=(Socket_handler &&) = delete;
  Socket_handler &operator=(const Socket_handler &) = delete;

private:
  struct Impl;

  std::unique_ptr<Impl> m_p_impl;
};
} // namespace Katas

#endif // #define SRC_SOCKET_HANDLER_H
