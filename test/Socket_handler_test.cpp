#include <Socket_handler.h>
#include <gtest/gtest.h>

#include <array>
#include <random>

namespace
{

class Socket_handler_test : public ::testing::Test
{
public:
  size_t m_bytes_read{};

  Socket_handler_test() = default;

  ~Socket_handler_test() override = default;

  static void socket_handler_callback(void *context, uint8_t *buffer,
                               size_t number_of_bytes)
  {
    auto p_this = static_cast<Socket_handler_test *>(context);
    p_this->m_bytes_read = number_of_bytes;
  }

  Socket_handler_test(Socket_handler_test &&) = delete;
  Socket_handler_test(const Socket_handler_test &) = delete;
  Socket_handler_test &operator=(Socket_handler_test &&) = delete;
  Socket_handler_test &operator=(const Socket_handler_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(Socket_handler_test, create_UDP)
{
  std::string IP_address = "127.0.0.1";
  int port = 8080;
  const size_t buffer_size = 1024;
  auto p_UUT = std::make_unique<Katas::Socket_handler>(
      Katas::socket_type::UDP_server, IP_address, port, buffer_size);
  ASSERT_NE(nullptr, p_UUT);
  ASSERT_TRUE(p_UUT->is_set_up());
}

TEST_F(Socket_handler_test, instantiate_UDP)
{
  std::string IP_address = "127.0.0.1";
  int port = 8080;
  const size_t buffer_size = 1024;
  Katas::Socket_handler server_UUT(Katas::socket_type::UDP_server, IP_address,
                                   port, buffer_size);
  ASSERT_TRUE(server_UUT.is_set_up());
  server_UUT.register_callback(socket_handler_callback,
                               static_cast<void *>(this));
  Katas::Socket_handler client_UUT(Katas::socket_type::UDP_client, IP_address,
                                   port, buffer_size);
  ASSERT_TRUE(server_UUT.is_set_up());
  const size_t size = 9;
  std::array<uint8_t, size> buffer{};
  ASSERT_TRUE(client_UUT.send_data(static_cast<uint8_t *>(buffer.data()), size));
  sleep(1);
  ASSERT_EQ(size, m_bytes_read);
}
} // namespace